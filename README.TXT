Vidali - Red Social Open Source, 2011 - 2014.

*NOTE: ENGLISH TRANSLATION COMING SOON...*

---
Equipo:

- CEO/Creador:
	Cristopher Caamana Gomez (cristo.c.mc@gmail.com | www.twitter.com/cristomc | www.vdli.wordpress.com)

- Webapp/Web Front-end:
	Moisés Lodeiro Santiago
	Haridian Rodriguez Pérez

- Back-end/Middleware:
	Iradiel García Perez
	José Isaac Hernandez Quintero

- Security:
	Moisés Lodeiro Santiago

- Mobile:
	Iradiel García Perez
	Cristopher Caamana Gómez


*ESTADO ACTUAL DEL PROYECTO: Beta Interna, No recomendado en entornos de uso cotidiano (de momento...).*

Vidali es un proyecto protegido bajo la licencia GPLv3. Puedes leer mas sobre la licencia en el archivo LICENSE.txt adjunto.

Tabla de contenidos:
|=======================
1- Resumen del proyecto.
2- Instalación.
3- Primeros usos.
4- Mejorar el código.

1- Resumen del proyecto.
|=======================
	Vidali surje como una alternativa de código abierto y gratuita a las actuales redes sociales existentes en internet.
	El principal objetivo del proyecto es permitir que cada usuario gestione su propio contenido.
	Además, al estar instalado en un servidor privado, se evita la dependencia de un dominio principal para el funcionamiento de la red (red descentralizada).
	Además, nos basamos en la geolocalización para ofrecer la mejorar la experiencia social en el mundo real, integrandonos con las ubicaciones que compartes y los servicios que puedan generarse alrededor de estos.

2- Instalación.
|=======================
	- Necesitas tener vidali.server en un servidor o en tu equipo (www.bitbucket.com/startdevs/vidali.server).
	- Esta es la webapp y se encuentra en una fase beta de desarrollo.
	- dentro del index.html debemos definir la url de "basedir" hacia el servidor que nos interesa conectar.
	- Enjoy! Crea tu usuario y comienza a explorar la red.
3- Bugs actuales.
|=======================
	- No permite modificar perfil.
	- No permite crear grupos.
	- Deshabilitados mensajes privados.
	- Bastanes elementos deshabilitados durante actualización del sistema.

4- Mejorar el código.
|=======================
Crea un Fork de este repositorio, mejoralo y envíanos commits, conviertete en un colaborador activo!

En esta web encontrarás todo lo relacionado al proyecto. También es el nodo oficial de Vidali.
htt://www.onvidali.com
Blog
htt://www.onvidali.com/blog

--------------------------------------------------------------------------------------------------------------------------------------------------
AGPLv3 - Vidali, 2010-2014.
