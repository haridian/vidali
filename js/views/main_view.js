/**
* @fileoverview Main lib of view, load the content of the main page.
*
* @author StartDevs
* @version 1.0
*/
var mainView = Backbone.View.extend({
	/** 
    @lends mainView.prototype
    */
    /**
     * <p>Saves a mainModel object, which will store basic data of the user and server url to make queries.<p>
     * @memberof mainView
     * @instance
     * @type {object}
     */
	model: new mainModel(),
    /**
     * <p>Saves a mapView object, which will draw the map on the UI.<p>
     * @memberof mainView
     * @instance
     * @type {object}
     */
    display: new view(),
	/**
     * Set id=container div as main HTML atribute where will render the UI.
     * @memberof modelView
     * @instance
     * @type {HTML}
     */
    el: $('#global-container'),
    /**
     * <p>This attribute will control the satus of the Updater box. Disabled by default<p>
     * @memberof modelView
     * @instance
     * @type {String}
     */
    updaterStatus: 'disabled',
    /**
     * <p>This attribute will control the satus of the Updater box. Disabled by default<p>
     * @memberof modelView
     * @instance
     * @type {String}
     */
    userActive:null,
     /**
     * <p>Set all listeners for actions in the main screen, it will change when the user make click on window's buttons.<p>
     * @memberof loginView
     * @instance
     * @type {string}
     */
    events:{
        "click .updateuser": "updateUser",
        "click .m-home": "loadHome",
        "click .all-updates": "loadHome",
        "click .my-updates": "loadUserPosts",
        "click .m-group": "loadGroup",
        "click .all-groups": "loadGroup",
        "click .my-groups": "loadUserGroup",
        "click .m-routes": "loadRoutes",
        "click .m-files": "loadFiles",
        "click .p-delete": "deletePost",
        "click .updater": "show_updater",
        "click .logout": "doLogout",
        "click .settings": "doSettings",
        "click .update-status": "updateStatus",
        "click .notify": "getNotifications",
        "click .accept": "acceptFriend",
        "click .reject": "rejectFriend",
        "click .block": "blockFriend",
        "click .join-group": "joinGroup",
        "click .leave-group": "leaveGroup",

    },
    /**
    * @class 
    * @name mainView
    * @classdesc Load the elements on the screen.
    * @constructs
    * @desc Gets geolocation's position, Load new user model which will save user active data.
    */
    initialize: function () {
        navigator.geolocation.getCurrentPosition(this.saveposition);
        var user_act = new userModel(jQuery.parseJSON(localStorage.getItem('user')));
        this.model.set('user_active',user_act);
        this.userActive = $.parseJSON(localStorage.getItem('user'));
    },
    /**
    * @public
    * @function render
    * @memberof mainView
    * @instance
    * @desc Draw main template and loads Home screen.
    */
    render: function () {
        template = _.template(main);
        this.$el.html(template);
        this.loadHome();
        $('#background').fadeOut(500);
    },
    /**
    * @public
    * @function deletePost
    * @memberof mainView
    * @instance
    * @desc Delete an user post.
    */
    deletePost: function (ev) {
        console.log('entraaa'); //capturar aqui el boton que manda el evento
        console.log(ev.target);
         var update = new postModel({
            id_user: this.userActive.id,
            id: $(ev.target).data('id_post').toString(),
            token: sessionStorage['session_auth'],
        });
        cUpdate = new postCollection();
        cUpdate.add(update);
        update.destroy();
        this.display.render();
    },
    /**
    * @public
    * @function show_updater
    * @memberof mainView
    * @instance
    * @desc Show/hide updater box, activated by listener.
    */
    show_updater: function(){
        if(this.updaterStatus == 'disabled'){
            $('#updater').fadeIn(100);
            this.updaterStatus = 'enabled';
        }
        else{
            $('#updater').fadeOut(100); 
            this.updaterStatus = 'disabled'; 
        }
    },
    /**
    * @public
    * @function show_updater
    * @memberof mainView
    * @instance
    * @desc Show/hide updater box, activated by listener.
    */
    updateStatus: function(){
        console.log('entra');
        console.log(this.userActive);
        var update = new postModel({
            id_user: this.userActive.id,
            nick: this.userActive.nick,
            id_group: null,
            date_published: null,
            text: $('#update_box').val(),
            lat: localStorage['latitude'],
            lon: localStorage['longitude'],
            token: sessionStorage['session_auth'],
            place_related: null,
        });
        var updates = new postCollection();
        updates.add(update);
        console.log(update);
        update.save({},{
            success: function(){
                displayMsg('Updated status! :)',"success");
            },
            error: function(){
                console.log('something failed');
            }
        });
        $('#updater').fadeOut(100);
    },
    /**
    * @public
    * @function show_updater
    * @memberof mainView
    * @instance
    * @desc Show/hide updater box, activated by listener.
    */
    getNotifications: function(){
        console.log('entra');
        console.log(this.userActive);
        var notify = new notifyModel({id: this.userActive.id});
        var notifications = new notifyCollection();
        notifications.add(notify);
        notify.fetch({
            success:function (data) {
                $('#notify').empty();
                $.each(data.attributes, function(index, value){
                    if(isNaN(value)){
                        $('#notify').append('<div id="notify-'+value.id_creator+'" class="notify-item"></div>');
                        $('#notify-'+value.id_creator).append(drawItem(value,"notification"));
                    }
                });
            },
            error: function(data){
                console.log("FAIL");
            }
        });
    },
    /**
    * @public
    * @function show_updater
    * @memberof mainView
    * @instance
    * @desc Show/hide updater box, activated by listener.
    */
    acceptFriend: function(ev){
        console.log('entraaa'); //capturar aqui el boton que manda el evento
        console.log(ev.target);
        var newFriend = new contactModel({
            id_creator: $(ev.target).data('id_creator').toString(),
            id_user: this.userActive.id,
            id_notification: $(ev.target).data('id_notification').toString()});
        cFriend = new contactCollection();
        cFriend.add(newFriend);
        newFriend.save({},{
            success: function(){
                console.log("lol"); // aqui hay que eliminar el div que ha sido aceptado
                $("#notify-"+$(ev.target).data('id_creator').toString()).fadeOut(300).remove();
            }
        });
    },
    /**
    * @public
    * @function show_updater
    * @memberof mainView
    * @instance
    * @desc Show/hide updater box, activated by listener.
    */
    joinGroup: function(ev){
        console.log('Join Group'); //capturar aqui el boton que manda el evento
        console.log(ev.target);
        var jointo = new groupsModel({
            userid: this.userActive.id,
            group: $(ev.target).data('group_name').toString()});
        cGroup = new groupsCollection();
        cGroup.add(jointo);
        jointo.save({},{
            success: function(){
                console.log("Join To Success"); // aqui hay que eliminar el div que ha sido aceptado
                //$("#notify-"+$(ev.target).data('id_creator').toString()).fadeOut(300).remove();
            }
        });
        this.display.render();
    },
    /**
    * @public
    * @function show_updater
    * @memberof mainView
    * @instance
    * @desc Show/hide updater box, activated by listener.
    */
    leaveGroup: function(ev){
        console.log('Leave Group'); //capturar aqui el boton que manda el evento
        console.log(ev.target);
        var gname = $(ev.target).data('group_name').toString().replace(' ', '~');
        var leave = new groupsModel({
            id: this.userActive.id + '#' + gname});
        cGroup = new groupsCollection();
        cGroup.add(leave);
        leave.destroy();
        this.display.render();
    },
    /**
    * @public
    * @function loadHome
    * @memberof mainView
    * @instance
    * @desc Clear map view, and load default map and submenu, set html atributes active for home screen.
    */
    loadHome: function(){
        $('#apps ul li').removeClass('active');
        $('#m-home').addClass('active');
        $('#submenu').empty();
        $('#submenu').append('<li class="active"><a href="#" data-toggle="tab" class="all-updates">All updates</a></li>');
        $('#submenu').append('<li><a href="#" data-toggle="tab" class="my-updates">My Updates</a></li>');
        this.display.setActive('Map');
        this.display.setPage('home');
        this.display.render();
    },
    /**
    * @public
    * @function loadHome
    * @memberof mainView
    * @instance
    * @desc Clear map view, and load default map and submenu, set html atributes active for home screen.
    */
    loadUserPosts: function(){
        $('#apps ul li').removeClass('active');
        $('#m-home').addClass('active');
        $('#submenu').empty();
        $('#submenu').append('<li><a href="#" data-toggle="tab" class="all-updates">All updates</a></li>');
        $('#submenu').append('<li class="active"><a href="#" data-toggle="tab" class="my-updates">My Updates</a></li>');
        this.display.setActive('Map');
        this.display.setPage('user-posts');
        this.display.render();
    },
    /**
    * @public
    * @function loadGroup
    * @memberof mainView
    * @instance
    * @desc Clear map view, and load default map and submenu, set html atributes active for group screen.
    */
    loadGroup: function(){
        $('#apps ul li').removeClass('active');
        $("#submenu").empty();
        $("#submenu").append('<li class="active"><a href="#" data-toggle="tab" class="all-groups">All Groups</a></li>');
        $("#submenu").append('<li><a href="#" data-toggle="tab" class="my-groups">My groups</a></li>');
        $('#m-group').addClass('active');
        $("#map").empty();
        $("#list").empty();
        this.display.setActive("Map");
        this.display.setPage("groups");
        this.display.render();
    },
    /**
    * @public
    * @function loadGroup
    * @memberof mainView
    * @instance
    * @desc Clear map view, and load default map and submenu, set html atributes active for group screen.
    */
    loadUserGroup: function(){
        $('#apps ul li').removeClass('active');
        $("#submenu").empty();
        $("#submenu").append('<li><a href="#" data-toggle="tab" class="all-groups">All Groups</a></li>');
        $("#submenu").append('<li class="active"><a href="#" data-toggle="tab" class="my-groups">My groups</a></li>');
        $('#m-group').addClass('active');
        $("#map").empty();
        $("#list").empty();
        this.display.setActive("Map");
        this.display.setPage("user-groups");
        this.display.render();
    },
    /**
    * @public
    * @function loadRoutes
    * @memberof mainView
    * @instance
    * @desc Clear map view, and load transport map and submenu, set html atributes active for routes screen.
    */
    loadRoutes: function(){
        $('#apps ul li').removeClass('active');
        $("#submenu").empty();
        $("#submenu").append('<li class="active"><a href="#" data-toggle="tab">My routes</a></li>');
        $("#submenu").append('<li><a href="#" data-toggle="tab">Public transport</a></li>');
        $("#submenu").append('<li><a href="#" data-toggle="tab">Hangouts</a></li>');
        $('#m-routes').addClass('active');
        $("#map").empty();
        $("#list").empty();
        this.display.setActive("Map");
        this.display.setPage("routes");
        this.display.render();

    },
    /**
    * @public
    * @function loadFiles
    * @memberof mainView
    * @instance
    * @desc Clear map view, and load default list and submenu, set html atributes active for files screen.
    */
    loadFiles: function(){
        $('#apps ul li').removeClass('active');
        $("#submenu").empty();
        $("#submenu").append('<li class="active"><a href="#" data-toggle="tab">Files</a></li>');
        $("#submenu").append('<li><a href="#" data-toggle="tab">Photos</a></li>');
        $("#submenu").append('<li><a href="#" data-toggle="tab">Videos</a></li>');
        $("#submenu").append('<li><a href="#" data-toggle="tab">Music</a></li>');
        $("#map").empty();
        $("#list").empty();
        $('#m-files').addClass('active');
        this.display.setActive("List");
        this.display.setPage("files");
        this.display.render();
    },
    updateUser: function(ev){
        console.log('cambiauser'); //capturar aqui el boton que manda el evento
        var birth= $("#ano").val();
        birth+="-"+$("#mes").val();
        birth+="-"+$("#dia").val();
        var user = new useryModel({
            id_user: this.userActive.id,
            email: $("#inputEmail").val(),
            nick: $("#inputNick").val(),
            name: $("#inputName").val(),
            sex: $('input[name="sex"]:checked').val(),
            birthdate: birth,
            default_location: $("#inputLocationDefault").val(),
            place: $("#inputPlace").val(),
            description: $("#inputDescription").val(),
            website: $("#inputWebsite").val(),
            privacy_level: $('input[name="privacy"]:checked').val()
        });
        cFriend = new userCollection();
        cFriend.add(user);
        user.save({},{
            success: function(){
                localStorage.setItem('user',JSON.stringify(user.attributes));
                displayMsg('Updated profile!',"success");
            }
        });
    },    
    /**
    * @public
    * @function doLogout
    * @memberof mainView
    * @instance
    * @desc Clear sessionStorage and localStorage, and reload all UI, in order to show login screen.
    */
    doLogout: function(){
        console.log("sale");
        localStorage.removeItem("latitude");
        localStorage.removeItem("longitude");
        localStorage.removeItem("session_auth");
        localStorage.removeItem("user");
        sessionStorage.removeItem("session_auth");
        location.reload();
    },
    /**
    * @public
    * @function doLogout
    * @memberof mainView
    * @instance
    * @desc Clear sessionStorage and localStorage, and reload all UI, in order to show login screen.
    */
    doSettings: function(){
        $('#apps ul li').removeClass('active');
        $("#submenu").empty();
        $("#submenu").append('<li class="active"><a href="#" data-toggle="tab">Profile</a></li>');
        $("#submenu").append('<li><a href="#" data-toggle="tab">Account</a></li>');
        $("#submenu").append('<li><a href="#" data-toggle="tab">Privacy</a></li>');
        $("#submenu").append('<li><a href="#" data-toggle="tab">Server</a></li>');
        console.log("entra");
        $("#map").empty();
        $("#list").empty();
        this.display.setActive("List");
        this.display.setPage("settings");
        this.display.render();
    },    /**
    * @public
    * @function saveposition
    * @memberof mainView
    * @instance
    * @desc Save the user's location.
    */
	saveposition : function(position){
	  localStorage['latitude'] = position.coords.latitude;
	  localStorage['longitude'] = position.coords.longitude;
	}
});
