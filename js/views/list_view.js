/**
* @fileoverview Map lib, draw the map on the main page.
*
* @author StartDevs
* @version 1.0
*/
var listView = Backbone.View.extend({
	/**
     * Define the HTML element asociated to the view.
     * @memberof mapView
     * @instance
     * @type {HTML}
     */
  el: $('#list'),
    /** 
    @lends loginView.prototype
    */
    /**
     * Saves the map model.
     * @memberof mapView
     * @instance
     * @type {object}
     */
  model: new ListModel(),
    /**
	 * @class 
	 * @name mapView
	 * @classdesc Load the map on the screen.
	 * @constructs
	 * @desc Load data about what kind of map is required.
	 */
  initialize: function (data) {
    if(data.maptype == "")
      this.model.set({default : "files"});
    else
      this.model.set({default : data.default});
  },
  /**
    * @public
    * @function render
    * @memberof mapView
    * @instance
    * @desc Draw map template, checking what kind of map needs. It also load geolocation engine.
    */
  render: function () {
    $('#container').html('');
    $('#container').append('<div id="list"></div> ');
    if (this.model.get("default") == 'files' ){
      console.log("load files");
      $("#list").append('<form id="setting" class="form-horizontal scroll" role="form"> \
        <div class="row"> \
          <div class="col-xs-6 col-md-4"> \
            <div class="col-xs-6 col-md-12 file"> \
              <div class="col-xs-6 col-md-4"> \
                <img src="img/prof_def_tb.jpg" class="img-responsive" alt="Responsive image"> \
              </div> \
              <div class="col-xs-6 col-md-8"> \
                Imagen.png \
              </div> \
            </div> \
            <div class="col-xs-6 col-md-12 file"> \
              <div class="col-xs-6 col-md-4"> \
                <img src="img/musica.jpg" class="img-responsive" alt="Responsive image"> \
              </div> \
              <div class="col-xs-6 col-md-8"> \
                Musica.mp3 \
              </div> \
            </div> \
            <div class="col-xs-6 col-md-12 file"> \
              <div class="col-xs-6 col-md-4"> \
                <img src="img/pdf.png" class="img-responsive" alt="Responsive image"> \
              </div> \
              <div class="col-xs-6 col-md-8"> \
                Archivo.pdf \
              </div> \
            </div> \
            <div class="col-xs-6 col-md-12 file"> \
              <div class="col-xs-6 col-md-4"> \
                <img src="img/video.png" class="img-responsive" alt="Responsive image"> \
              </div> \
              <div class="col-xs-6 col-md-8"> \
                Video.avi \
              </div> \
            </div> \
          </div> \
          <div class="col-xs-12 col-sm-6 col-md-8"> \
            <div class="col-xs-12 col-sm-6 col-md-9 col-md-offset-3"> \
              <img src="img/multimedia.png" class="img-responsive" alt="Responsive image"> \
            </div> \
            <div class="col-xs-12 col-sm-6 col-md-9 col-md-offset-3"> \
              Informacion del archivo multimedia \
            </div> \
            <div class="col-xs-12 col-sm-6 col-md-8 col-md-offset-4"> \
              <a class="fast-backward" data-toggle="tooltip" data-placement="top" title="" data-original-title="Fast Backward"> \
                <span class="glyphicon glyphicon-fast-backward"></span> \
              </a> \
              <a class="backward" data-toggle="tooltip" data-placement="top" title="" data-original-title="Fast Backward"> \
                <span class="glyphicon glyphicon-backward"></span> \
              </a> \
              <a class="play" data-toggle="tooltip" data-placement="top" title="" data-original-title="Fast Backward"> \
                <span class="glyphicon glyphicon-play"></span> \
              </a> \
              <a class="pause" data-toggle="tooltip" data-placement="top" title="" data-original-title="Fast Backward"> \
                <span class="glyphicon glyphicon-pause"></span> \
              </a> \
              <a class="forward" data-toggle="tooltip" data-placement="top" title="" data-original-title="Fast Backward"> \
                <span class="glyphicon glyphicon-forward"></span> \
              </a> \
               <a class="fast-forward" data-toggle="tooltip" data-placement="top" title="" data-original-title="Fast Backward"> \
                <span class="glyphicon glyphicon-fast-forward"></span> \
              </a> \
            </div> \
          </div> \
        </div> \
      </form>');
    }
    if (this.model.get("default") == 'settings' ){
      console.log("load settings");
      var user_info = jQuery.parseJSON(localStorage.getItem('user'));
      $("#list").append('<form id="setting" class="form-horizontal row" role="form"> \
        <div class="col-md-5"> \
          <div class="form-group"> \
            <div class="contenedor"> \
              <div id="marcoVistaPrevia"> \
                <div class="col-sm-10"> \
                  <img style="max-width: 150px; max-height: 150px;" id="vistaPrevia" src="" alt="" /> \
                </div> \
              </div> \
            </div> \
            <div id="botonera"> \
              <div class="col-sm-10"> \
                <input id="archivo" type="file" accept="image/*"></input> \
                <input id="cancelar" type="button" value="Cancelar"></input> \
              </div> \
            </div> \
          </div> \
          <div class="form-group"> \
            <label for="inputEmail3" class="col-sm-2 control-label">Email</label> \
            <div class="col-sm-8"> \
              <input type="email" class="form-control" id="inputEmail" value="'+user_info.Email+'" placeholder="Email"> \
            </div> \
          </div> \
          <div class="form-group"> \
            <label for="inputEmail3" class="col-sm-2 control-label">Nick</label> \
            <div class="col-sm-8"> \
              <input type="nick" class="form-control" id="inputNick" value="'+user_info.nick+'" placeholder="Nick"> \
            </div> \
          </div> \
          <div class="form-group"> \
            <label for="inputEmail3" class="col-sm-2 control-label">Name</label> \
            <div class="col-sm-8"> \
              <input type="name" class="form-control" id="inputName" value="'+user_info.name+'" placeholder="Name"> \
            </div> \
          </div> \
          <div class="form-group"> \
            <label for="inputEmail3" class="col-sm-2 control-label">Sex</label> \
            <div class="col-sm-8"> \
              <input type="radio" name="sex" value="male" checked>Male<br> \
              <input type="radio" name="sex" value="female">Female \
            </div> \
          </div> \
          <div class="form-group"> \
            <label for="inputEmail3" class="col-sm-2 control-label">Birthday</label> \
            <div class="col-sm-8"> \
              <input type="number" id="dia" name="dia" min="1" max="31" value="24"> \
              <input type="number" id="mes" name="mes" min="1" max="12" value="10"> \
              <input type="number" id="ano" name="ano" min="1900" max="2014" value="1991"> \
            </div> \
          </div> \
        </div> \
        <div class="col-md-5"> \
          <div class="form-group"> \
            <label for="inputLocationDefault3" class="col-sm-4 control-label">Location Default</label> \
            <div class="col-sm-8"> \
              <input type="LocationDefault" class="form-control" id="inputLocationDefault" placeholder="Location Default"> \
            </div> \
          </div> \
          <div class="form-group"> \
            <label for="inputPlace3" class="col-sm-4 control-label">Place</label> \
            <div class="col-sm-8"> \
              <input type="Place" class="form-control" id="inputPlace" placeholder="Place"> \
            </div> \
          </div> \
          <div class="form-group"> \
            <label for="inputDescription3" class="col-sm-4 control-label">Description</label> \
            <div class="col-sm-8 "> \
              <input type="Description" class="form-control" id="inputDescription" value="'+user_info.description+'" placeholder="Description"> \
            </div> \
          </div> \
          <div class="form-group"> \
            <label for="inputWebsite3" class="col-sm-4 control-label">Website</label> \
            <div class="col-sm-8"> \
              <input type="Website" class="form-control" id="inputWebsite" placeholder="Website"> \
            </div> \
          </div> \
          <label for="inputPrivacyLevel3" class="col-sm-4 control-label">Privacy Level</label> \
          <div class="col-sm-8"> \
              <input type="radio" name="privacy" value="High" checked>High<br> \
              <input type="radio" name="privacy" value="Medium">Medium<br> \
              <input type="radio" name="privacy" value="Low">Low \
            </div> \
          <div class="form-group"> \
            <label for="inputWebsite3" class="col-sm-4 control-label">Save Changes</label> \
            <div class="col-sm-8"> \
              <a href="#" class="btn btn-default updateuser">Save</a> \
            </div> \
          </div> \
        </div> \
      </form>');
    }
    //Este string contiene una imagen de 1px*1px color blanco
    window.imagenVacia 

    window.mostrarVistaPrevia = function mostrarVistaPrevia() {

        var Archivos, Lector;

        //Para navegadores antiguos
        if (typeof FileReader !== "function") {
            jQuery('#infoNombre').text('[Vista previa no disponible]');
            jQuery('#infoTamaño').text('(su navegador no soporta vista previa!)');
            return;
        }

        Archivos = jQuery('#archivo')[0].files;
        if (Archivos.length > 0) {

            Lector = new FileReader();
            Lector.onloadend = function(e) {
                var origen, tipo;

                //Envia la imagen a la pantalla
                origen = e.target; //objeto FileReader
                //Prepara la información sobre la imagen
                tipo = window.obtenerTipoMIME(origen.result.substring(0, 30));

                jQuery('#infoNombre').text(Archivos[0].name + ' (Tipo: ' + tipo + ')');
                jQuery('#infoTamaño').text('Tamaño: ' + e.total + ' bytes');
                //Si el tipo de archivo es válido lo muestra, 
                //sino muestra un mensaje 
                if (tipo !== 'image/jpeg' && tipo !== 'image/png' && tipo !== 'image/gif') {
                    jQuery('#vistaPrevia').attr('src', window.imagenVacia);
                    alert('El formato de imagen no es válido: debe seleccionar una imagen JPG, PNG o GIF.');
                } else {
                    jQuery('#vistaPrevia').attr('src', origen.result);
                    window.obtenerMedidas();
                }

            };
            Lector.onerror = function(e) {
                console.log(e)
            }
            Lector.readAsDataURL(Archivos[0]);

        } else {
            var objeto = jQuery('#archivo');
            objeto.replaceWith(objeto.val('').clone());
            jQuery('#vistaPrevia').attr('src', window.imagenVacia);
            jQuery('#infoNombre').text('[Seleccione una imagen]');
            jQuery('#infoTamaño').text('');
        };


    };

    //Lee el tipo MIME de la cabecera de la imagen
    window.obtenerTipoMIME = function obtenerTipoMIME(cabecera) {
        return cabecera.replace(/data:([^;]+).*/, '\$1');
    }

    //Obtiene las medidas de la imagen y las agrega a la 
    //etiqueta infoTamaño
    window.obtenerMedidas = function obtenerMedidas() {
        jQuery('<img/>').bind('load', function(e) {

            var tamaño = jQuery('#infoTamaño').text() + '; Medidas: ' + this.width + 'x' + this.height;

            jQuery('#infoTamaño').text(tamaño);

        }).attr('src', jQuery('#vistaPrevia').attr('src'));
    }

    jQuery(document).ready(function() {

        //Cargamos la imagen "vacía" que actuará como Placeholder
        jQuery('#vistaPrevia').attr('src', window.imagenVacia);

        //El input del archivo lo vigilamos con un "delegado"
        jQuery('#botonera').on('change', '#archivo', function(e) {
            window.mostrarVistaPrevia();
        });

        //El botón Cancelar lo vigilamos normalmente
        jQuery('#cancelar').on('click', function(e) {
            var objeto = jQuery('#archivo');
            objeto.replaceWith(objeto.val('').clone());

            jQuery('#vistaPrevia').attr('src', window.imagenVacia);
            jQuery('#infoNombre').text('[Seleccione una imagen]');
            jQuery('#infoTamaño').text('');
        });

});
  }
});
