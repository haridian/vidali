/**
* @fileoverview Map lib, draw the map on the main page.
*
* @author StartDevs
* @version 1.0
*/
var mapView = Backbone.View.extend({
	/**
     * Define the HTML element asociated to the view.
     * @memberof mapView
     * @instance
     * @type {HTML}
     */
  el: $('#map'),
    /** 
    @lends loginView.prototype
    */
    /**
     * Saves the map model.
     * @memberof mapView
     * @instance
     * @type {object}
     */
  model: new mapModel(),
    /**
	 * @class 
	 * @name mapView
	 * @classdesc Load the map on the screen.
	 * @constructs
	 * @desc Load data about what kind of map is required.
	 */
  initialize: function (data) {
    if(data.maptype == "")
      this.model.set({map : "default"});
    else
      this.model.set({map : data.maptype});
  },
  /**
    * @public
    * @function render
    * @memberof mapView
    * @instance
    * @desc Draw map template, checking what kind of map needs. It also load geolocation engine.
    */
  render: function () {
      $('#container').html('');
      $('#container').append('<div id="map"></div> ');
      var user_info = jQuery.parseJSON(localStorage.getItem('user'));
      var label = draw_popup(user_info,"active_user");
  	if((this.model.get("map") == "default") || (this.model.get("map") == "user-posts") ){   
      console.log("carga home");   
      var mapa = L.map('map');
      var basicMap = L.tileLayer('http://otile4.mqcdn.com/tiles/1.0.0/osm/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="www.openstreetmap.org/copyright">OpenStreetMap</a>',
        minZoom: 3,
        maxZoom: 18
      });
      basicMap.addTo(mapa);
      var post = new postModel({id: user_info.id});
      var posts = new postCollection();
      posts.add(post);
      if(this.model.get("map") == "default")
        posts.seturl('wall');
      else
        posts.seturl('user-posts');        
      var fmarker = L.AwesomeMarkers.icon({
        icon: 'glyphicon-user',
        markerColor: 'green'
      });
      var wall = L.layerGroup();
      post.fetch({
        success: function(data){
          $.each(data.attributes, function(index, value){
            if(isNaN(value)){
              var lat = parseFloat(value.lat);
              var lon = parseFloat(value.lon);
              wall.addLayer(
                L.marker([lat,lon], {icon: fmarker}).bindPopup(draw_popup(value,"user_post",user_info.id))
              );
            }
          });
          wall.addTo(mapa);
        },
        error: function(){
          console.log("fail receiving updates");
        }
      });
  	}
    if(this.model.get("map") == "routes"){
      var mapa = L.map('map');
      var transportMap = L.tileLayer('http://{s}.tile.opencyclemap.org/transport/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="www.openstreetmap.org/copyright">OpenStreetMap</a>',
        minZoom: 3,
        maxZoom: 18
      });
      var post = new postModel();
      var posts = new postCollection();
      posts.add(post);
      post.fetch({
        data: {id: user_info.id, type: "routes"},
        sucess: function(){
          console.log("all updates received");
        },
        error: function(){
          console.log("fail receiving updates");
        }
      });
      transportMap.addTo(mapa);

    }
    if((this.model.get("map") == "groups") || (this.model.get("map") == "user-groups")){
      var mapa = L.map('map');
      var basicMap = L.tileLayer('http://otile4.mqcdn.com/tiles/1.0.0/osm/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="www.openstreetmap.org/copyright">OpenStreetMap</a>',
        minZoom: 3,
        zoom: 14,
        maxZoom: 18
      });
      //if(this.model.get("map") == "groups")
        //var post = new postModel();
      //else
      var post = new postModel({id: user_info.id});
      var posts = new postCollection();
      posts.add(post);
      var fmarker = L.AwesomeMarkers.icon({
        icon: 'glyphicon-asterisk',
        markerColor: 'orange'
      });
      var MyGrMarker = L.AwesomeMarkers.icon({
        icon: 'glyphicon-asterisk',
        markerColor: 'green'
      });
      var wall = L.layerGroup();
      if(this.model.get("map") == "groups")
        posts.seturl('groups');
      else
        posts.seturl('my-groups');
      post.fetch({
        success: function(data){
          $.each(data.attributes, function(index, value){
            if(isNaN(value)){
              var lat = parseFloat(value.lat);
              var lon = parseFloat(value.long);
              if (value.joined){
                wall.addLayer(
                  L.marker([lat,lon], {icon: MyGrMarker}).bindPopup(draw_popup(value,"groups",user_info.id))
                );
              } else{
                wall.addLayer(
                  L.marker([lat,lon], {icon: fmarker}).bindPopup(draw_popup(value,"groups",user_info.id))
                );
              }
            }
          });
          wall.addTo(mapa);
        },
        error: function(){
          console.log("fail receiving updates");
        }
      });
      basicMap.addTo(mapa);
    }

    try{
      mapa.locate({watch:true, setView: true, maxZoom: 18,timeout: 5000, enableHighAccuracy: true});
    }
    catch(e){
      console.log("falla aqui");
    }
    var marker = null;
    var circle = null;

    function onLocationFound(e) {
      var radius = e.accuracy / 2;
      if(marker){
        marker.setLatLng(e.latlng);
        circle.setLatLng(e.latlng);
      }
      else{
        marker = L.marker(e.latlng)
            .bindPopup(label+"Near  " + radius + " meters.").openPopup().addTo(mapa);
        circle = L.circle(e.latlng, radius).addTo(mapa);
      }
      localStorage['latitude'] = e.latlng.lat;
      localStorage['longitude'] = e.latlng.lng;
    }

    mapa.on('locationfound', onLocationFound);

    function onLocationError(e) {
        console.log(e.message);
    }

    mapa.on('locationerror', onLocationError);

  }
});
