//seting base url conection
//var baseurl = 'http:www.onvidali.com/Vidali';

var baseurl = 'http://127.0.0.1';

var save_ip = function(ip){
    user_ip = ip;
}
$.get('http://jsonip.com',function (res) {
    save_ip(res.ip);
});

/**
* Global functions
*/
var draw_popup = function(data, type,user_id){
  var label;
  if(type == "active_user"){
    label = '<div class="post-box">'+
                '<div class="row">'+
                  '<div class="col-xs-4">'+
                    '<img src="http://www.gravatar.com/avatar/ecc897cd5da72d6c018d57d4c4628671.png" alt="..."  width=80 height=80 class="img-circle">'+
                  '</div>'+
                  '<div class="col-xs-8"><h4>'+
                    data.name+
                  '</h4></div>'+
                  '<div class="col-xs-4">'+
                    data.nick+
                  '</div>'+
                  '<div class="col-xs-4">'+
                    data.birthdate+
                  '</div>'+
                  '<div class="col-xs-8">'+
                    data.description+
                  '</div>'+
                '</div>'+ 
                '<div class="col-xs-12">'+
                  '<br><p align="center"><i>Ejemplo de ultimo estado ;)</i></p>'+
                '</div>'+  
              '</div>'
              ;
  }
  if(type == "user_post"){
    label = '<div class="post-box">'+
                '<div class="row">'+
                  '<div class="col-xs-12">'+
                      '<div class="row">'+
                          '<div class="col-xs-4">'+
                            '<img src="http://www.gravatar.com/avatar/ecc897cd5da72d6c018d57d4c4628671.png" alt="..."  width=80 height=80 class="img-rounded">'+
                          '</div>'+
                          '<div class="col-xs-8"><h4>'+
                            data.name+' ('+data.nick+')'+
                          '</h4></div>'+
                          '<div class="col-xs-8">'+
                            data.text+
                          '</div>'+
                      '</div>'+
                  '</div>';
                  if(user_id != data.id_user){
                    label += '<div class="col-xs-12">'+
                        '<button type="button" class="btn btn-primary">Add Friend</button>'+
                        '<button type="button" class="btn btn-default">Like</button>'+
                        '<button type="button" class="btn btn-default">Meet</button>'+
                    '</div>';
                  }
                  else{
                    label += '<div class="col-xs-12">'+
                        '<button type="button" class="btn btn-primary p-delete" data-id_post="' + data.id + '">Delete</button>'+
                        '<button type="button" class="btn btn-default">Edit</button>'+
                        '<button type="button" class="btn btn-default">Hide</button>'+
                    '</div>';

                  }
                '</div>'+ 
              '</div>'
              ;
  }
  if(type=="groups"){
      label = '<div style="width: 250px;">'+
                '<div class="row">'+
                  '<div class="col-xs-12"><h4>'+
                    data.name+
                  '</h4></div>'+
                  '<div class="col-xs-4">'+
                    '<img src="http://www.gravatar.com/avatar/ecc897cd5da72d6c018d57d4c4628671.png" alt="..."  width=80 height=80 class="img-rounded">'+
                  '</div>'+
                  '<div class="col-xs-8">'+
                    data.description+
                  '</div>'+
                  '<div class="col-xs-8">'+
                    data.users+
                  '</div>'+
                  '<div class="col-xs-8">';
                    if (data.joined){
                      label += '<button type="button" class="btn btn-warning leave-group" data-group_name="'+data.name+'">Leave</button>';
                    }
                    else {
                      label += '<button type="button" class="btn btn-success join-group" data-group_name="'+data.name+'">Join</button>';
                    }
                  '</div>'+
                '</div>'+ 
              '</div>'
              ;
  }
  return label;
}

var drawItem = function(data,type){
  var item;
  //hay que crear un modelo por cada tipo de usuario
  //usuario activo: contiene toda la informacion,debe ser fuente de información
  //contacto/otro usuario: contiene las operaciones con las que trabajar
  //enviarle un mensaje
  //agregar/eliminar/bloquear a esa persona.
  if(type =="notification"){
    item = '<div class="row notification-'+data.id+'">'+
      '<img src="http://www.gravatar.com/avatar/ecc897cd5da72d6c018d57d4c4628671.png" alt="'+data.image+'" height=60 class="col-xs-4">'+
      '<div class="col-xs-8"><b>'+data.nick+'</b></div>';
      if(data.type == "friend"){
        item += '<div class="col-xs-8">Added you as a friend!</div>';
        item += '<div class="col-xs-8"><button class="accept btn btn-success" data-id_creator="'+data.id_creator+'" data-id_notification="'+data.id+'"><span class="glyphicon glyphicon-ok"></span></button>'+
                '<button class="reject btn btn-danger"><span class="glyphicon glyphicon-remove"></span></button>'+
                '<button class="block btn btn-warning"><span class="glyphicon glyphicon-ban-circle"></span></button></div>';
      }
      if(data.type == "reply"){
        item += '<div class="col-xs-8">Made a Reply on your Status!</div>';

      }
      if(data.type == "message"){
        item += '<div class="col-xs-8">Sent you a message.</div>';

      }
  }
  return item;
}

var displayMsg = function(msg,type){
      clearTimeout(msgTimeout);
      $("#alert").empty();
      $("#alert").append("<strong>INFO: </strong>");
      $("#alert").fadeOut("fast",function(){
          $("#alert").attr("class","alert " + (type ? "alert-"+type : "alert-message"));
          $("#alert").append(" "+msg);
          $("#alert").fadeIn("fast");
          msgTimeout = setTimeout(function(){$('#alert').fadeOut();},4000);
      });
  }
