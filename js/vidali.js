/**
* @fileoverview Main lib of Vidali, start the engine and load all UI.
*
* @author StartDevs
* @version 1.0
*/
var Vidali = Backbone.View.extend({
 /** 
    @lends Vidali.prototype
*/
    /**
     * <p>Saves the active view of the app.</p>
     * <p>In order to load properly the UI, view will store a object of loginView or mainView, that will draw the screen.</p>
     * @memberof Vidali
     * @instance
     * @type {object}
     */
    view: null,
    /**
    * @class 
    * @name Vidali
    * @classdesc Main View of the app, it loads UI and check compatibilities and status of the app.
    * @constructs
    * @desc start the engine
    */
    initialize: function(){
    	//Detect compatibility browser
	    if(!this.isCompatible()){
    	    alert('Navegador no soportado');
    	    document.location.href='http://www.firefox.com';
    	}
    	//Check login status
		if(!this.isLoged()){
            this.view = new loginView({ id: "LoginView"});
            this.listenTo(this.view.model,'sync',this.change);
        }
        else{
            this.view = new mainView({ id: "mainView"});
        }
        //draw the screen
        this.render();
    },
    /**
    * @public
    * @function render
    * @memberof Vidali
    * @instance
    * @desc Draws the view in the current screen. 
    */
    render: function(){
        this.view.render();
    },
    /**
    * @public
    * @function change
    * @memberof Vidali
    * @instance
    * @desc <p>Change function is triggered by a listener on the constructor.</p>
            <p>This function will load mainView object if session is started. </p>
    */
    change: function(){
        if(sessionStorage.getItem("session_auth")){
            this.view = new mainView({ id: "mainView"});
            this.render();
        }
    },
    /**
    * @public
    * @function isCompatible
    * @memberof Vidali
    * @instance
    * @desc Check if navigator has HTML5 compatibility. 
    * @returns {boolean}
    */
    isCompatible : function(){
		return localStorage && navigator.geolocation;
    },
    /**
    * @public
    * @function isSaved
    * @memberof Vidali
    * @instance
    * @desc Check if session is saved for future auto-login. 
    * @returns {boolean}
    */
    isSaved: function(){
		return localStorage.getItem('session-id');
    },
    /**
    * @public
    * @function isLoged
    * @memberof Vidali
    * @instance
    * @desc Checks if is the user loged. 
    * @returns {boolean}
    */
    isLoged: function(){
        if(localStorage.getItem('session_auth'))
            sessionStorage.setItem('session_auth',localStorage.getItem('session_auth'));
        return sessionStorage.getItem('session_auth')? 1 : 0;
    },
});

/**
 * Global var, stores a object of Vidali class.
 * @type {object}
 */
var vdl;
var popover;

$(document).ready(function(){
    $("#background").fadeIn(500);
	$.when(vdl = new Vidali({ id: "Vidaliapp"}))
           .done(function() {
                $('#background').fadeOut(500);
            });
   $('[data-toggle="tooltip"]').tooltip();
   popover = $('[data-toggle="popover"]').popover({content: '<div id="notify">All Clear :)</div>', html:true});
    $('input[type=file]').change( function(){
        console.log( $(this).val() );
    });
    $('input[type=date]').datepicker(); 
    return false;
});

// //Este string contiene una imagen de 1px*1px color blanco
// window.imagenVacia 

// window.mostrarVistaPrevia = function mostrarVistaPrevia() {

//     var Archivos, Lector;

//     //Para navegadores antiguos
//     if (typeof FileReader !== "function") {
//         jQuery('#infoNombre').text('[Vista previa no disponible]');
//         jQuery('#infoTamaño').text('(su navegador no soporta vista previa!)');
//         return;
//     }

//     Archivos = jQuery('#archivo')[0].files;
//     if (Archivos.length > 0) {

//         Lector = new FileReader();
//         Lector.onloadend = function(e) {
//             var origen, tipo;

//             //Envia la imagen a la pantalla
//             origen = e.target; //objeto FileReader
//             //Prepara la información sobre la imagen
//             tipo = window.obtenerTipoMIME(origen.result.substring(0, 30));

//             jQuery('#infoNombre').text(Archivos[0].name + ' (Tipo: ' + tipo + ')');
//             jQuery('#infoTamaño').text('Tamaño: ' + e.total + ' bytes');
//             //Si el tipo de archivo es válido lo muestra, 
//             //sino muestra un mensaje 
//             if (tipo !== 'image/jpeg' && tipo !== 'image/png' && tipo !== 'image/gif') {
//                 jQuery('#vistaPrevia').attr('src', window.imagenVacia);
//                 alert('El formato de imagen no es válido: debe seleccionar una imagen JPG, PNG o GIF.');
//             } else {
//                 jQuery('#vistaPrevia').attr('src', origen.result);
//                 window.obtenerMedidas();
//             }

//         };
//         Lector.onerror = function(e) {
//             console.log(e)
//         }
//         Lector.readAsDataURL(Archivos[0]);

//     } else {
//         var objeto = jQuery('#archivo');
//         objeto.replaceWith(objeto.val('').clone());
//         jQuery('#vistaPrevia').attr('src', window.imagenVacia);
//         jQuery('#infoNombre').text('[Seleccione una imagen]');
//         jQuery('#infoTamaño').text('');
//     };


// };

// //Lee el tipo MIME de la cabecera de la imagen
// window.obtenerTipoMIME = function obtenerTipoMIME(cabecera) {
//     return cabecera.replace(/data:([^;]+).*/, '\$1');
// }

// //Obtiene las medidas de la imagen y las agrega a la 
// //etiqueta infoTamaño
// window.obtenerMedidas = function obtenerMedidas() {
//     jQuery('<img/>').bind('load', function(e) {

//         var tamaño = jQuery('#infoTamaño').text() + '; Medidas: ' + this.width + 'x' + this.height;

//         jQuery('#infoTamaño').text(tamaño);

//     }).attr('src', jQuery('#vistaPrevia').attr('src'));
// }

// //jQuery(document).ready(function() {

//     //Cargamos la imagen "vacía" que actuará como Placeholder
//     jQuery('#vistaPrevia').attr('src', window.imagenVacia);

//     //El input del archivo lo vigilamos con un "delegado"
//     jQuery('#archivo').on('change', '#archivo', function(e) {
//         console.log("Entramos?");
//         window.mostrarVistaPrevia();
//     });

//     //El botón Cancelar lo vigilamos normalmente
//     jQuery('#cancelar').on('click', function(e) {
//         var objeto = jQuery('#archivo');
//         objeto.replaceWith(objeto.val('').clone());

//         jQuery('#vistaPrevia').attr('src', window.imagenVacia);
//         jQuery('#infoNombre').text('[Seleccione una imagen]');
//         jQuery('#infoTamaño').text('');
//     });

//});
/*$('body').on('click', function (e) { Hide because just hide de div, but actually block is there
    e.preventDefault();
    $('[data-toggle="popover"]').each(function () {
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});*/
