var notify = '<div class="popover notify" id="notify" tabindex="-1" role="dialog"> \
	<div class="arrow"></div> \
	<h3 class="popover-title">Notifications</h3> \
	<div class="popover-content"> \
        <div class="row"> \
			<div class="col-xs-12 col-md-12 well"><a href="#">Haridian Rodriguez</a></div> \
			<div class="col-xs-12 col-md-12 well"><a href="#">Iradiel García Pérez</a></div> \
			<div class="col-xs-12 col-md-12 well"><a href="#">Jose Hernandez</a></div> \
        </div>\
        <div class="row"> \
        	<div class="col-xs-3 col-md-3"> \
				<button type="button" class="btn btn-default btn-block">Read it!</button> \
			</div> \
        	<div class="col-xs-3 col-md-3"> \
				<button type="button" class="btn btn-default btn-block">Mute</button> \
			</div> \
        	<div class="col-xs-3 col-md-3"> \
				<button type="button" class="btn btn-default btn-block">Clear</button> \
			</div> \
        	<div class="col-xs-3 col-md-3"> \
				<button type="button" class="btn btn-default btn-block">Settings</button> \
			</div> \
        </div>\
    </div> \
</div>';