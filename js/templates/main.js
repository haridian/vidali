var main = '<header class="navbar navbar-inverse" role="navigation"> \
	<div style="float: left;"> \
		<img src="img/v-mini.png"> \
	</div> \
	<ul class="nav nav-pills"> \
		<li id="m-update"> \
			<a href="#" data-toggle="tooltip" data-placement="bottom" title data-original-title="Update Status" class="updater"> \
				<span class="glyphicon glyphicon-edit"></span> \
			</a> \
		</li> \
		<li id="m-msg"> \
			<a href="#" data-toggle="modal" data-placement="bottom" title data-original-title="Messages" class="contentLink" data-target="#myModal"> \
				<span class="glyphicon glyphicon-envelope"></span> \
			</a> \
		</li> \
		<li id="m-notify"> \
			<a href="#" class="notify" data-toggle="popover" data-placement="bottom" data-original-title="Notifications"> \
				<span class="glyphicon glyphicon-bell"></span> \
			</a> \
		</li> \
		<li id="m-set" class="dropdown pull-right"> \
			<a id="drop" href="#" data-toggle="dropdown"> \
				<span class="glyphicon glyphicon-plus"></span> \
				<b class="caret"></b> \
			</a> \
			<ul id="menu" class="dropdown-menu" role="menu" aria-labelledby="drop"> \
				<li> \
					<a href="#" data-toggle="tooltip" data-placement="bottom" title data-original-title="Choose betwen map mode or list mode"> \
						<span class="glyphicon glyphicon-th"></span> \
						Clasic Mode \
					</a> \
				</li> \
				<li> \
					<a href="#" data-toggle="tooltip" data-placement="bottom" title data-original-title="show your current position in your updates or use your \'secure position\'"> \
						<span class="glyphicon glyphicon-eye-close"></span> \
						Invisible Mode \
					</a> \
				</li> \
				<li id="m-set"> \
					<a href="#" class="settings" data-toggle="tooltip" data-placement="bottom" title data-original-title="Change options of your profiles, your account or your privacy."> \
						<span class="glyphicon glyphicon-wrench"></span> \
						Settings \
					</a> \
				</li> \
				<li> \
					<a href="#" class="logout"data-toggle="tooltip" data-placement="bottom" title data-original-title="End your session."> \
						<span class="glyphicon glyphicon-off"></span> \
						Logout \
					</a> \
				</li> \
			</ul> \
		</li> \
	</ul> \
</header> \
<ul id="submenu" class="navbar-inner2"></ul> \
<div id="container"> \
	<div id="list"></div> \
</div> \
<nav id="apps" class="navbar navbar-fixed-bottom" role="navigation"> \
	<ul class="nav nav-pills"> \
		<li id="m-home"> \
			<a class="m-home" data-toggle="tooltip" data-placement="top" title="Home Page"> \
				<span class="glyphicon glyphicon-home"></span> \
			</a> \
		</li> \
		<li id="m-group"> \
			<a class="m-group" data-toggle="tooltip" data-placement="top" title="Groups"> \
				<span class="glyphicon glyphicon-globe"></span> \
			</a> \
		</li> \
		<li id="m-routes"> \
			<a class="m-routes" data-toggle="tooltip" data-placement="top" title="Routes"> \
				<span class="glyphicon glyphicon-road"></span> \
			</a> \
		</li> \
		<li id="m-files"> \
			<a class="m-files" data-toggle="tooltip" data-placement="top" title="Files"> \
				<span class="glyphicon glyphicon-folder-close"></span> \
			</a> \
		</li> \
	</ul> \
</nav> \
<footer class="footer"> \
	<p class="pull-right">Powered by Vidali. 2014 <img src="img/html5.png"><img src="img/agpl.png"></p> \
</footer>\
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> \
  <div class="modal-dialog"> \
    <div class="modal-content"> \
      <div class="modal-header"> \
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> \
        <h4 class="modal-title" id="myModalLabel">Messages</h4> \
      </div> \
      <div class="modal-body"> \
        <div class="row"> \
        	<div class="col-xs-4 col-md-4"> \
				<div class="col-xs-12 col-md-12 well"><a href="#">Haridian Rodriguez</a></div> \
				<div class="col-xs-12 col-md-12 well"><a href="#">Iradiel García Pérez</a></div> \
				<div class="col-xs-12 col-md-12 well"><a href="#">Jose Hernandez</a></div> \
        	</div> \
        	<div class="col-xs-8 col-md-8 well"> CHAT DIALOG<br><br><br><br><br><br><br><br><br></div> \
        </div>\
      </div> \
      <div class="modal-footer"> \
        <div class="row"> \
        	<div class="col-xs-4 col-md-4"> \
				<div class="col-xs-12 col-md-12 ">Attach files | Send file</div> \
        	</div> \
        	<div class="col-xs-8 col-md-8"> \
        		<div class="row"> \
		        	<div class="col-xs-9 col-md-9"> \
						<input type="text" class="form-control" placeholder="Type your message..."> \
					</div> \
		        	<div class="col-xs-3 col-md-3"> \
						<button type="button" class="btn btn-default btn-block">Send</button> \
					</div> \
				</div> \
        	</div> \
        </div>\
      </div> \
    </div><!-- /.modal-content --> \
  </div><!-- /.modal-dialog --> \
</div><!-- /.modal --> \
<form id="updater" class="form-inline" style="display: none;"> \
    <textarea id="update_box" name="update" class="post-text form-control" rows="3" placeholder="Actualiza tu estado"></textarea> \
    <a href="#" class="btn btn-default"><span class="glyphicon glyphicon-star"></span></a> \
    <a href="#" class="btn btn-default"><span class="glyphicon glyphicon-upload"></span></a> \
    <a href="#" class="update-status btn btn-primary pull-right"><span class="glyphicon glyphicon-ok"></span></a> \
</form> \
';