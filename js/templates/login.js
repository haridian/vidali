var login=' <div class="container"> \
  <div class="row"> \
    <div class="col-xs-12 col-md-6 col-md-offset-3" style="color: #fff;"> \
      <div style="text-align: center; color: #fff; margin-bottom: 10px;"> \
        <h2>Welcome to Vidali</h2> \
        <img class="image" src="img/vidali-icon.png" alt="Vidali Social Network" height="200" width="200" style="margin-bottom: 10px;"> \
      </div> \
      <form id="login-form" class="form-horizontal" autocomplete="on"> \
        <div class="form-group"> \
          <div class="col-lg-12"> \
            <input id="email" name="email" type="email" class="form-control input-lg" placeholder="Email" required> \
          </div> \
        </div> \
        <div class="form-group"> \
          <div class="col-lg-12"> \
            <input id="password" name="password" type="password" class="form-control input-lg" placeholder="Password" required> \
          </div> \
        </div> \
        <div class="form-group"> \
          <div class="col-lg-12"> \
            <input type="checkbox" class="pull-left" id="remember" name="remember"> Remember me \
            <a href="#" class="pull-right" data-toggle="modal" data-target="#Recover"> Forgoten password?</a> \
          </div> \
        </div> \
        <div class="form-group"> \
          <div class="col-lg-12"> \
            <button href="#" type="button" class="login btn btn-success btn-lg btn-block">Login</button> \
            <button type="button" class="btn btn-primary btn-lg btn-block" data-toggle="modal" data-target="#myModalRegister">Register</button><br> \
          </div> \
        </div> \
      </form> \
      <ul class="nav nav-pills nav-justified"> \
        <li> \
          <a href="https://github.com/vidali/Vidali">Get involved</a> \
        </li> \
        <li> \
          <a href="http://www.onvidali.com/blog">Blog</a> \
        </li> \
        <li> \
          <a href="https://github.com/vidali/Vidali/wiki">Wiki</a> \
        </li> \
        <li> \
          <a href="http://www.twitter.com/vidalisn">@Vidalisn</a> \
        </li> \
      </ul> \
    </div> \
  </div> \
</div> \
<div class="modal fade" id="Recover" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> \
  <div class="modal-dialog"> \
    <div class="modal-content"> \
      <div class="modal-header"> \
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> \
        <h4 class="modal-title" id="myModalLabel">Recover Password</h4> \
      </div> \
      <div class="modal-body"> \
        <form class="form-horizontal" role="form"> \
          <div class="form-group"> \
            <label for="inputEmail3" class="col-sm-2 control-label">Put your email</label> \
            <div class="col-sm-10"> \
              <input type="email" class="form-control" id="inputEmail3" placeholder="Email"> \
            </div> \
          </div> \
        </form> \
      </div> \
      <div class="modal-footer"> \
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> \
        <button type="button" class="forgot btn btn-primary">Recover</button> \
      </div> \
    </div><!-- /.modal-content --> \
  </div><!-- /.modal-dialog --> \
</div><!-- /.modal --> \
<div class="modal fade" id="myModalRegister" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> \
  <div class="modal-dialog"> \
    <div class="modal-content"> \
      <div class="modal-header"> \
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> \
        <h4 class="modal-title" id="myModalLabel">Register</h4> \
      </div> \
      <div class="modal-body"> \
        <form class="form-horizontal" role="form"> \
          <div class="form-group"> \
            <label for="reg-nick" class="col-sm-2 control-label">Nick</label> \
            <div class="col-sm-10"> \
              <input type="text" class="form-control" id="reg-nick" placeholder="Nick" required> \
            </div> \
          </div> \
          <div class="form-group"> \
            <label for="reg-name" class="col-sm-2 control-label">Name</label> \
            <div class="col-sm-10"> \
              <input type="text" class="form-control" id="reg-name" placeholder="Name" required> \
            </div> \
          </div> \
          <div class="form-group"> \
            <label for="reg-email" class="col-sm-2 control-label">Email</label> \
            <div class="col-sm-10"> \
              <input type="email" class="form-control" id="reg-email" placeholder="Email" required> \
            </div> \
          </div> \
          <div class="form-group"> \
            <label for="reg-password" class="col-sm-2 control-label">Password</label> \
            <div class="col-sm-10"> \
              <input type="password" class="form-control" id="reg-password" placeholder="Password" required> \
            </div> \
          </div> \
          <div class="form-group"> \
            <label for="reg-password2" class="col-sm-2 control-label">Repeat Password</label> \
            <div class="col-sm-10"> \
              <input type="password" class="form-control" id="reg-password2" placeholder="Repeat Password" required> \
            </div> \
          </div> \
          <div class="form-group"> \
            <div class="col-sm-10"> \
                <input type="checkbox" id="tos" name="tos" required> \
                I agree <a href="#">Vidali\'s TOS</a><br> \
            </div> \
            <div class="col-sm-10"> \
                <input type="checkbox" id="allow_ads" name="allow_ads"> \
                I accept ads on my wall and sending stadistic data of my usage. \
            </div> \
          </div> \
        </form> \
      </div> \
      <div class="modal-footer"> \
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> \
        <button type="button" class="register btn btn-primary">Register</button> \
      </div> \
    </div><!-- /.modal-content --> \
  </div><!-- /.modal-dialog --> \
</div><!-- /.modal -->';