var files = '<form class="form-horizontal scroll" role="form"> \
        <div class="row"> \
          <div class="col-xs-6 col-md-4"> \
            <div class="col-xs-6 col-md-12 file"> \
              <div class="col-xs-6 col-md-4"> \
                <img src="img/prof_def_tb.jpg" class="img-responsive" alt="Responsive image"> \
              </div> \
              <div class="col-xs-6 col-md-8"> \
                Imagen.png \
              </div> \
            </div> \
            <div class="col-xs-6 col-md-12 file"> \
              <div class="col-xs-6 col-md-4"> \
                <img src="img/musica.jpg" class="img-responsive" alt="Responsive image"> \
              </div> \
              <div class="col-xs-6 col-md-8"> \
                Musica.mp3 \
              </div> \
            </div> \
            <div class="col-xs-6 col-md-12 file"> \
              <div class="col-xs-6 col-md-4"> \
                <img src="img/pdf.png" class="img-responsive" alt="Responsive image"> \
              </div> \
              <div class="col-xs-6 col-md-8"> \
                Archivo.pdf \
              </div> \
            </div> \
            <div class="col-xs-6 col-md-12 file"> \
              <div class="col-xs-6 col-md-4"> \
                <img src="img/video.png" class="img-responsive" alt="Responsive image"> \
              </div> \
              <div class="col-xs-6 col-md-8"> \
                Video.avi \
              </div> \
            </div> \
          </div> \
          <div class="col-xs-12 col-sm-6 col-md-8"> \
            <div class="col-xs-12 col-sm-6 col-md-9 col-md-offset-3"> \
              <img src="img/multimedia.png" class="img-responsive" alt="Responsive image"> \
            </div> \
            <div class="col-xs-12 col-sm-6 col-md-9 col-md-offset-3"> \
              Informacion del archivo multimedia \
            </div> \
            <div class="col-xs-12 col-sm-6 col-md-8 col-md-offset-4"> \
              <a class="fast-backward" data-toggle="tooltip" data-placement="top" title="" data-original-title="Fast Backward"> \
                <span class="glyphicon glyphicon-fast-backward"></span> \
              </a> \
              <a class="backward" data-toggle="tooltip" data-placement="top" title="" data-original-title="Fast Backward"> \
                <span class="glyphicon glyphicon-backward"></span> \
              </a> \
              <a class="play" data-toggle="tooltip" data-placement="top" title="" data-original-title="Fast Backward"> \
                <span class="glyphicon glyphicon-play"></span> \
              </a> \
              <a class="pause" data-toggle="tooltip" data-placement="top" title="" data-original-title="Fast Backward"> \
                <span class="glyphicon glyphicon-pause"></span> \
              </a> \
              <a class="forward" data-toggle="tooltip" data-placement="top" title="" data-original-title="Fast Backward"> \
                <span class="glyphicon glyphicon-forward"></span> \
              </a> \
               <a class="fast-forward" data-toggle="tooltip" data-placement="top" title="" data-original-title="Fast Backward"> \
                <span class="glyphicon glyphicon-fast-forward"></span> \
              </a> \
            </div> \
          </div> \
        </div> \
      </form>';