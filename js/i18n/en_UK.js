// ESTADO

var UPDATE_STATUS = "Update Status";

// PAGINA DE INICIO

var HOME_PAGE = "Home Page";
var ALL_UPDATES = "All Updates";
var MY_UPDATES = "My Updates";

// PAGINA DE GRUPOS

var GROUPS = "Groups";
var ALL_GROUPS = "All Groups";
var MY_GROUPS = "My Groups";

// PAGINA DE RUTAS

var ROUTES = "Routes";
var MY_ROUTES = "My Routes";
var PUBLIC_TRANSPORT = "Public Transport";
var HANGOUTS = "Hangouts";

// PAGINA DE ARCHIVOS

var FILES = "Files";
var PHOTOS = "Photos";
var VIDEOS = "Videos";
var MUSIC = "Music";

// AJUSTES

var CLASIC MODE = "Clasic Mode";
var INVISIBLE MODE = "Invisible Mode";
var SETTINGS = "Settings";
var LOGOUT = "Logout";

// MENSAJES

var MESSAGES = "Messages";
var ATTACH_FILES = "Attach Files";
var SEND_FILE = "Send File";
var SEND = "Send";

// PANTALLA DE LOGIN

var LOGIN = "Login";
var EMAIL = "Email";
var PASSWORD = "Password";
var REMEMBER_ME = "Remember me";
var FORGOTEN_PASSWORD = "Forgoten Password";
var REGISTER = "Register";
var GET_INVOLVED = "Get involved";
var FEEDBACK = "Feedback";