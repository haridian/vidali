// ESTADO

var UPDATE_STATUS = "Actualizar Estado";

// PAGINA DE INICIO

var HOME_PAGE = "Pagina de Inicio";
var ALL_UPDATES = "Todas las Actualizaciones";
var MY_UPDATES = "Mis Actualizaciones";

// PAGINA DE GRUPOS

var GROUPS = "Grupos";
var ALL_GROUPS = "Todos los Grupos";
var MY_GROUPS = "Mis Grupos";

// PAGINA DE RUTAS

var ROUTES = "Rutas";
var MY_ROUTES = "Mis Rutas";
var PUBLIC_TRANSPORT = "Transporte Publico";
var HANGOUTS = "Reuniones";

// PAGINA DE ARCHIVOS

var FILES = "Archivos";
var PHOTOS = "Fotos";
var VIDEOS = "Vídeos";
var MUSIC = "Música";

// AJUSTES

var CLASIC MODE = "Modo Clásico";
var INVISIBLE MODE = "Modo Invisible";
var SETTINGS = "Ajustes";
var LOGOUT = "Cerrar Sesión";

// MENSAJES

var MESSAGES = "Mensajes";
var ATTACH_FILES = "Adjuntar Archivos";
var SEND_FILE = "Enviar Archivo";
var SEND = "Enviar";

// PANTALLA DE LOGIN

var LOGIN = "Iniciar Sesión";
var EMAIL = "Correo";
var PASSWORD = "Contraseña";
var REMEMBER_ME = "Recordarme";
var FORGOTEN_PASSWORD = "Password olvidado";
var REGISTER = "Registro";
var GET_INVOLVED = "Únete";
var FEEDBACK = "Feedback";