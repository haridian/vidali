var postModel = Backbone.Model.extend();

var postCollection = Backbone.Collection.extend({
	url: baseurl+"/vidali.server/api.php/userpost/",
	seturl: function(type){
		if(type == "wall"){
			this.url = baseurl+"/vidali.server/api.php/getwall/";
		}
		if(type == "user-posts"){
			this.url = baseurl+"/vidali.server/api.php/getuserposts/";
		}
		if(type == "groups"){
			this.url = baseurl+"/vidali.server/api.php/getallgroups/";			
		}
		if(type == "my-groups"){
			this.url = baseurl+"/vidali.server/api.php/getgroups/";			
		}
		if(type == "routes"){
			this.url = baseurl+"/vidali.server/api.php/gettransport/";
		}
	}
});