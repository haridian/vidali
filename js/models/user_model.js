var userModel = Backbone.Model.extend({
	defaults: {
		id: null,
        email: null,
        nick: null,
        name: null,
        sex: null,
        birthdate: null,
        default_location: null,
        place: null,
        //image: null,
        description: null,
        website: null,
        privacy_level: null,
        view_mode: null,
        hide_mode: null,
        mail_notify: null,
        n_contacts: null,
        n_groups: null
    },
    url: baseurl+"/vidali.server/api.php/user/add",
});
//TODO: CAMBIAR ESTO DESPUES DE LA REUNION
var useryModel = Backbone.Model.extend({
    url: baseurl+"/vidali.server/api.php/user/update",
});

var contactModel = Backbone.Model.extend({});

var contactCollection = Backbone.Collection.extend({
    url: baseurl+"/vidali.server/api.php/contact"
});

var userCollection = Backbone.Collection.extend({
    url: baseurl+"/vidali.server/api.php/user"
});