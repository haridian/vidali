var view = Backbone.View.extend({
    Map: new mapView({maptype: "default"}),
    List: new listView({default: "settings"}),
    Active: "Map",
    Page: "default",
    el: $('#container'),
    getActive: function(){
        return this.Active;
    },
    setActive: function(type){
        this.Active = type;
    },
    setPage: function(type){
        this.Page = type;
    },
	initialize: function(type){
        //ocultar inactivo, por defecto lista
        $('#list').hide();
    },
    render: function(){
        //cargar render de activo
        if(this.Active == "Map"){
            if (this.Page == "home"){
                this.Map = new mapView({maptype: "default"});
            }
            else if (this.Page == "user-posts"){
                this.Map = new mapView({maptype: "user-posts"});
            }
            else if (this.Page == "groups"){
                this.Map = new mapView({maptype: "groups"});
            }
            else if (this.Page == "user-groups"){
                this.Map = new mapView({maptype: "user-groups"});
            }
            else if (this.Page == "routes"){
                this.Map = new mapView({maptype: "routes"});
            }
            this.Map.render();
        }
        else{
          if (this.Page == "files"){
              this.List = new listView({default: "files"});
          }
          else if (this.Page == "settings"){
              this.List = new listView({default: "settings"});
          }
            this.List.render();
        }
    }
});
